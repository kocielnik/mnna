# Build

```bash
cd typesetting && make
```

# Test the manuscript

```bash
mdl mnna.md
```

# Resources

1. WIWIKWLH, Stephen Diehl, https://github.com/sdiehl/wiwinwlh
